from django.db import models
from django.shortcuts import reverse
from django.contrib.auth.models import User

# TODO: constraints!
# TODO: validation -   > 0 and so on


class Dataset(models.Model):
    # qw
    TYPES = (
        ('Apr', 'Approximation'),
        ('Cls', 'Classification')
    )

    name = models.CharField(max_length=50, verbose_name='Название датасета')
    type = models.CharField(max_length=3, choices=TYPES, verbose_name='Тип')
    description = models.TextField(blank=True, null=True, verbose_name='Описание')  # qw blank, null
    file_link = models.CharField(max_length=150, verbose_name='Ссылка на публикацию')
    instances = models.IntegerField(verbose_name='Количество экземпляров')
    features = models.IntegerField(verbose_name='Количество признаков')
    classes = models.IntegerField(null=True, blank=True, verbose_name='Количество классов')  # т.к. в датасетах для аппроксимации нет классов

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        url = 'datasets:dataset_detail_url'
        return reverse(url, kwargs={'id': self.id})

    # для отображения в панели администрирования
    class Meta:
        verbose_name_plural = "Датасеты"
        verbose_name = "Датасет"


class Result(models.Model):
    # qw CASCADE?
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='User', verbose_name='Пользователь') # todo User (import?)
    dataset_id = models.ForeignKey(Dataset, on_delete=models.CASCADE, related_name='Dataset', verbose_name='Датасет')
    algorithm = models.CharField(max_length=100, verbose_name='Алгоритм') # qw max_length
    # qw blank, null
    # типа чтобы можно было внести только тестовые результаты, если отсутствуют результаты обучения, и наоборот?
    learning_result = models.FloatField(blank=True, null=True, verbose_name='Результаты при обучении', )
    test_result = models.FloatField(blank=True, null=True, verbose_name='Результаты при тестировании')
    # FloatField? решила всё-таки сделать целым
    features = models.IntegerField(verbose_name='Количество признаков')
    rules = models.IntegerField(verbose_name='Количество правил')

    # методика эксперимента
    # METHODS = ()? choices? text or char?
    method = models.TextField(verbose_name='Методика эксперимента')

    def __str__(self):
        return f'{self.user_id.last_name} {self.user_id.first_name} - {self.dataset_id.name} - {self.algorithm}'

    def get_delete_url(self):
        return reverse('datasets:result_delete_url', kwargs={'id': self.id})

    def get_update_url(self):
        return reverse('datasets:result_update_url', kwargs={'id': self.id})

    # для отображения в панели администрирования
    class Meta:
        verbose_name_plural = "Результаты"
        verbose_name = "Результат"
        # ordering = ['learning_result']

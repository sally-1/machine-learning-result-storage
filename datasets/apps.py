from django.apps import AppConfig


class DatasetsConfig(AppConfig):
    name = 'datasets'
    # для отображения в панели администрирования
    verbose_name = "Датасеты и результаты"

from django.urls import path

from .views import * # datasets_list_view

app_name = 'datasets'

urlpatterns = [
    # path(),
    # path('', datasets_list_view, name='datasets-list'),
    path('approximation_datasets/', approx_datasets_list_view, name='approximation_datasets-list'),
    path('classification_datasets/', class_datasets_list_view, name='classification_datasets-list'),

    # # qw или сделать общий урл datasets/<int:id> - но тогда не сделаешь обратный переход через ../?
    # path('approximation_datasets/<int:id>/', dataset_detail_view, name='approximation_dataset_detail_url'),
    # path('classification_datasets/<int:id>/', dataset_detail_view, name='classification_dataset_detail_url'),
    path('<int:id>/', dataset_detail_view, name='dataset_detail_url'),

    path('<int:id>/results/', results_list_view, name='results_list_url'),    # qw /
    path('result/<int:id>/update/', ResultUpdateView.as_view(), name='result_update_url'),
    path('result/<int:id>/delete/', ResultDeleteView.as_view(), name='result_delete_url'),
    path('result/create/', ResultCreateView.as_view(), name='result_create_url'),

]

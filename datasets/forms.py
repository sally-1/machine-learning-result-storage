from django import forms
from django.forms.widgets import Input

from .models import Result


class ResultModelForm(forms.ModelForm):
    class Meta:
        model = Result
        exclude = ['user_id']
        # fields = ['algorithm',
        #           'learning_result',
        #           'test_result',
        #           'features',
        #           'rules',
        #           'method',
        #           # 'dataset_id',
        #           # 'user_id'
        #           ]
        widgets = {'dataset_id': forms.HiddenInput()}
        # widgets = {'dataset_id': Input(attrs={'hidden': 'True'}), 'user_id': Input(attrs={'hidden': 'True'})}

    # def clean_user_id(self, request):
    #     # data = self.cleaned_data['user_id']
    #     return request.user

    # def clean_dataset_id(self):
    #     pass
# def __init__(self, user, dataset, *args, **kwargs):
    #     super(ResultModelForm, self).__init__(*args, **kwargs)
    #     self.user_id = user
    #     self.dataset_id = dataset

from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from .models import Dataset, Result
from django.views.generic import View, CreateView
from .forms import ResultModelForm
from django.contrib.auth.models import User


@login_required
def class_datasets_list_view(request):
    datasets = Dataset.objects.filter(type='Cls')
    context = {'datasets': datasets}
    return render(request, 'datasets/classification_datasets_list.html', context)


@login_required
def approx_datasets_list_view(request):
    datasets = Dataset.objects.filter(type='Apr')
    context = {'datasets': datasets}
    return render(request, 'datasets/approximation_datasets_list.html', context)


@login_required
def dataset_detail_view(request, id):
    dataset = get_object_or_404(Dataset, id=id)
    # результаты сортируются по полю learning_result в порядке по убыванию (от большего к меньшему - от лучшего к худшему)
    results = Result.objects.filter(dataset_id=dataset.id).order_by('-learning_result')
    context = {'dataset': dataset, 'results': results}
    template_name = 'datasets/base_detail_view.html'  # qw
    if dataset.type == 'Apr':
        template_name = 'datasets/approximation_dataset_detail.html'
    elif dataset.type == 'Cls':
        template_name = 'datasets/classification_dataset_detail.html'
    return render(request, template_name, context)


# Results views
def owns_result(request, owner_id):
    """
    Проверяет, является ли запрашивающий пользователь владельцем запрашиваемого результата
    Если нет, то выкидывает 403 ошибку
    :param request:
    :param owner_id:
    :return:
    """
    print(request.user.id)
    print(owner_id)
    if request.user.id == owner_id:
        return
    else:
        raise PermissionDenied


class ResultCreateView(LoginRequiredMixin, View):
    template_name = 'datasets/result_create.html'
    form_class = ResultModelForm

    def get(self, request):
        dataset_name = request.GET['dataset']
        ds = Dataset.objects.get(name__iexact=dataset_name)
        form = self.form_class(initial={'dataset_id': ds})
        return render(request, self.template_name, {'form': form, "dataset": ds})

    def post(self, request):
        bound_form = self.form_class(request.POST)

        if bound_form.is_valid():
            result = bound_form.save(commit=False)
            result.user_id = request.user
            result.save()
            return redirect(result.dataset_id)
        return render(request, self.template_name, context={'form': bound_form})


# TODO: доступ только авторизованным пользователям + только пользователям, которым принадлежит результат
class ResultDeleteView(View):
    model = ResultModelForm
    template_name = 'datasets/result_delete.html'

    def get(self, request, id):
        result = get_object_or_404(Result, id=id)
        owns_result(request, result.user_id.id)
        return render(request, self.template_name, {'result': result})

    def post(self, request, id):
        result = get_object_or_404(Result, id=id)
        owns_result(request, result.user_id.id)
        redir_dataset = result.dataset_id
        result.delete()
        return redirect(redir_dataset)


# TODO: доступ только авторизованным пользователям + только пользователям, которым принадлежит результат
class ResultUpdateView(View):
    model = Result
    model_form = ResultModelForm
    template = 'datasets/result_update.html'

    def get(self, request, id):
        result = get_object_or_404(self.model, id=id)
        owns_result(request, result.user_id.id)
        bound_form = self.model_form(instance=result)
        return render(request, self.template, context={'form': bound_form, self.model.__name__.lower(): result})

    def post(self, request, id):
        result = get_object_or_404(self.model, id=id)
        owns_result(request, result.user_id.id)
        bound_form = self.model_form(request.POST, instance=result)

        if bound_form.is_valid():
            new_result = bound_form.save()
            return redirect(new_result.dataset_id)
        return render(request, self.template, context={'form': bound_form, self.model.__name__.lower(): result})


@login_required
def results_list_view(request, id):
    results = Result.objects.filter(user_id=id)
    user = get_object_or_404(User, id=id)
    # user = User.objects.get(id=id)
    return render(request, 'datasets/results_list.html', {'results': results, 'user': user})



# def owns_result(view_func):
#     def wrapper(request, owner_id):
#         if request.user.id == owner_id:
#             pass
#         else:
#             raise PermissionDenied
#     return wrapper

from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm


class AuthUserForm(AuthenticationForm):#, forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'password']
        # widgets = {
        # 'username': forms.TextInput(attrs={'class': 'form-control'}),
        # 'password': forms.TextInput(attrs={'class': 'form-control'})
        # }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # set html attributes for the fields
            self.fields[field].widget.attrs['class'] = 'form-control'
        # self.fields['username'].label = 'Username' # работает

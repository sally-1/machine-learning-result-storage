from django.urls import path


from .views import *

app_name = 'users'

urlpatterns = [
    path('login/', MyLoginView.as_view(), name='login_url'),
    # # path('register/', , name='register_url'), # qw регистрация только через админа?
    path('logout/', my_logout, name='logout_url'),
]

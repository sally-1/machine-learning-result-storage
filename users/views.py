from django.shortcuts import render, resolve_url
from django.shortcuts import redirect
from django.urls import reverse
from django.contrib.auth.views import LoginView, LogoutView
from .forms import AuthUserForm
from django.contrib.auth import logout


class MyLoginView(LoginView):
    template_name = 'users/login.html'
    form_class = AuthUserForm

    # redirect_field_name = 'next'

    # print(self.request.GET.get('next', 'kek'))
    # print(self.request.GET.dict)
    # success_url = '/'

    # def get(self, request):
    #     # print(self.success_url)
    #     # self.success_url = request.GET.get('next', '/')
    #     # print(self.success_url)
    #     return super(LoginView, self).get(request)

    def get_success_url(self):
        #     redirect_to = self.request.POST.get(
        #         self.redirect_field_name,
        #         self.request.GET.get(self.redirect_field_name, '')
        #     )
        #     print(redirect_to)
        #     return redirect_to
        return reverse('main_page_url')
    #
    # def get_next_page(self):
    #     if self.next_page is not None:
    #         next_page = resolve_url(self.next_page)


# todo сообщение о выходе и тп
def my_logout(request):
    logout(request)
    return redirect('/')

# class MyLogoutView(LogoutView):
#     template_name = 'users/logout.html'
#     form_class =
#     success_url =
